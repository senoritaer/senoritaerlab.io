---
title: 推荐书籍
date: 2021-02-10 15:11:17
tags: [resource]
---

## English
 - 英语魔法师之语法俱乐部 - *旋元佑*
 - 英语常用词疑难用法手册 - *陈用仪*
 - 新东方英语语法新思维 - *张满胜*
 - Merriam-Webster's Vocabulary Builder - *Mary Wood Cornog / Merriam-Webster*
 - Raymond-Murphy-English-Grammar-in-Use-Cambridge-University-Press-2019 - *Raymond Murphy / Cambridge University Press*
 
## Mathematics
 - 普林斯顿微积分读本 - *Adrian Banner / 杨爽 / 赵晓婷 / 高璞*

 暂时就这么多，以后找到一点写一点。
