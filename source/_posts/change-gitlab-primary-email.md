---
title: 更改Gitlab主邮箱
date: 2022-02-21 15:11:17
tags: [gitlab,other]
---

由于域名快到期了，gitlab的主邮箱是这域名的邮箱，就想着更改一下吧，结果找了半天设置也没找到设置主邮箱的地方，搜一下才发现遇到这个问题的人还不少，而且也有人提过issue，最早的一次是在五年前[https://forum.gitlab.com/t/profile-settings-emails/312](https://forum.gitlab.com/t/profile-settings-emails/312)，一年前又有人提，但是[issue](https://gitlab.com/gitlab-org/gitlab/-/issues/255903)直接被关闭了，不过在这条issue下面我找到了方法。

> https://gitlab.com/-/profile if you edit 'Email' it changes primary. 
> It's just ridiculously non-obviously labeled and a free-form input field rather than e.g. selecting the email from the ones you added already.

给出解决方法的这位老哥跟我的感受一样啊，so ridiculous。

虽然是小问题，但还是要记录一下，方便后面其他同样遇到的这个问题的朋友。



