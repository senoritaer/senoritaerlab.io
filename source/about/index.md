---
title: about
date: 2017-09-20 23:00:53
---


本博客基于hexo搭建，托管于gitlab，感谢Cloudflare提供的大量免费资源。

主要记录生活和工作中的遇到的一些问题以及解决办法，作为经验和知识的积累，留给以后做参考，不希望自己已经踩过的坑再踩一次。

## contact ##

me@gentlehu.com 

如有任何疑问，欢迎邮件联系。
<hr>

<!-- ![night exposure](https://res.cloudinary.com/akame-moe/image/upload/uncategory/photo-1549138144-42ff3cdd2bf8.jpg) -->

<!--width="850" height="637"-->
<!--
<video controls> 
  <source src="https://res.cloudinary.com/akame-moe/video/upload/v1618428214/yuanbingyan.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
-->

